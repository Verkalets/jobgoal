class IndustryController < ApplicationController
  def forestry
  end

  def hunting
  end

  def automotive
  end

  def banking
  end

  def computing
  end

  def technology
  end

  def defense
  end

  def energy
  end

  def arts
  end

  def food
  end

  def health
  end

  def insurance
  end

  def manufacturing
  end

  def marketing
  end

  def media
  end

  def mining
  end

  def personal
  end

  def services
  end

  def biotechnology
  end

  def services
  end

  def estate
  end

  def retail
  end

  def transportation
  end

  def travel
  end

  def waste
  end

  def remediation
  end
end
