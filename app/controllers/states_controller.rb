class StatesController < ApplicationController



  def alabama
    @job_pages = JobPages.all
  end

  def alaska
    @job_pages = JobPages.all
  end

  def arizona
    @job_pages = JobPages.all
  end

  def arkansas
    @job_pages = JobPages.all
  end

  def california
    @job_pages = JobPages.all
  end

  def colorado
    @job_pages = JobPages.all
  end

  def connecticut
    @job_pages = JobPages.all
  end

  def delaware
    @job_pages = JobPages.all
  end

  def florida
    @job_pages = JobPages.all
  end

  def georgia
    @job_pages = JobPages.all
  end

  def hawaii
    @job_pages = JobPages.all
  end

  def idaho
    @job_pages = JobPages.all
  end

  def illinois
    @job_pages = JobPages.all
  end

  def indiana
    @job_pages = JobPages.all
  end

  def iowa
    @job_pages = JobPages.all
  end

  def kansas
    @job_pages = JobPages.all
  end

  def kentucky
    @job_pages = JobPages.all
  end

  def louisiana
    @job_pages = JobPages.all
  end

  def maine
    @job_pages = JobPages.all
  end

  def maryland
    @job_pages = JobPages.all
  end

  def massachusetts
    @job_pages = JobPages.all
  end

  def michigan
    @job_pages = JobPages.all
  end

  def minnesota
    @job_pages = JobPages.all
  end

  def mississippi
    @job_pages = JobPages.all
  end

  def missouri
    @job_pages = JobPages.all
  end

  def montana
    @job_pages = JobPages.all
  end

  def nebraska
    @job_pages = JobPages.all
  end

  def nevada
    @job_pages = JobPages.all
  end

  def new_hampshire
    @job_pages = JobPages.all
  end

  def new_mexico
    @job_pages = JobPages.all
  end

  def new_jersey
    @job_pages = JobPages.all
  end

  def new_york
    @job_pages = JobPages.all
  end

  def north_carolina
    @job_pages = JobPages.all
  end

  def north_dakota
    @job_pages = JobPages.all
  end

  def ohio
    @job_pages = JobPages.all
  end

  def oklahoma
    @job_pages = JobPages.all
  end

  def pennsylvania
    @job_pages = JobPages.all
  end

  def rhode_island
    @job_pages = JobPages.all
  end

  def south_carolina
    @job_pages = JobPages.all
  end

  def south_dakota
    @job_pages = JobPages.all
  end

  def tennessee
    @job_pages = JobPages.all
  end

  def texas
    @job_pages = JobPages.all
  end

  def utah
    @job_pages = JobPages.all
  end

  def vermont
    @job_pages = JobPages.all
  end

  def virginia
    @job_pages = JobPages.all
  end

  def washington
    @job_pages = JobPages.all
  end

  def washington_dc
    @job_pages = JobPages.all
  end

  def west_virginia
    @job_pages = JobPages.all
  end

  def wisconsin
    @job_pages = JobPages.all
  end

  def wyoming
    @job_pages = JobPages.all
  end
end
