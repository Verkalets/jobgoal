require 'rubygems'
require 'open-uri'
require 'nokogiri'

class JobController < ApplicationController



  before_action :set_post, only: [:show]
  before_action :set_page, only: [:show_page]

  def landing
    @job_pages = JobPages.all
    @posts = Post.all.order_by(created_at: :desc).limit(3)
    @advertising = Advertising.all.order_by(order: :desc).limit(3)

    $search = params[:search]
    $location = params[:location]

    session[:search] = params[:search]
    session[:location] = params[:location]

    if session[:search].to_s.match(/\s/)
      session[:search] = session[:search].to_s.gsub!(" ", "+")
    end

    if session[:location].to_s.match(/\s/)
      session[:location] = session[:location].to_s.gsub!(" ", "+")
    end

    if params[:location] == ''
      @search_finded = true
      redirect_to search_result_path(params[:search])
    elsif params[:search] == ''
      @search_finded = false
      redirect_to location_result_path(params[:location])
    elsif !params[:search].nil? && !params[:location].nil?
      redirect_to result_path(params[:search],params[:location])
    end

  end


  def search_result
    @job_pages = JobPages.all
    @posts = Post.all.order_by(created_at: :desc).limit(3)
    @advertising = Advertising.all.order_by(order: :desc).limit(3)

    session[:search] = params[:search]

    if session[:search].to_s.match(/\s/)
      session[:search] = session[:search].to_s.gsub!(" ", "+")
    end


    @url="http://api.simplyhired.com/a/jobs-api/xml-v2/q-"+session[:search].to_s+"?pshid=68487&ssty=2&cflg=r&jbd=jobgoal.jobamatic.com"

    @doc = Nokogiri::HTML(open(@url))
    @result = @doc.css("rs r").to_s
    @total_result = @doc.css("tr").to_s
    @total_result_2 = @doc.css("tr").to_s
    @current_page_count = @doc.css("si").to_s

    if @total_result.nil?
      @total_result
    else
      @total_result = @total_result
    end
  end


  def location_result
    @job_pages = JobPages.all
    @posts = Post.all.order_by(created_at: :desc).limit(3)
    @advertising = Advertising.all.order_by(order: :desc).limit(3)

    session[:location] = params[:location]

    if session[:location].to_s.match(/\s/)
      session[:location] = session[:location].to_s.gsub!(" ", "+")
    end


    @url="http://api.simplyhired.com/a/jobs-api/xml-v2/q-"+session[:location].to_s+"?pshid=68487&ssty=2&cflg=r&jbd=jobgoal.jobamatic.com"

    @doc = Nokogiri::HTML(open(@url))
    @result = @doc.css("rs r").to_s
    @total_result = @doc.css("tr").to_s
    @total_result_2 = @doc.css("tr").to_s
    @current_page_count = @doc.css("si").to_s

    if @total_result.nil?
      @total_result
    else
      @total_result = @total_result
    end
  end

  def home
    @job_pages = JobPages.all
    $search = params[:search]
    $location = params[:location]

    session[:search] = params[:search]
    session[:location] = params[:location]

    if session[:search].to_s.match(/\s/)
      session[:search] = session[:search].to_s.gsub!(" ", "+")
    end

    if session[:location].to_s.match(/\s/)
      session[:location] = session[:location].to_s.gsub!(" ", "+")
    end

    if session[:search].nil?
      session[:search]=params[:search]
    else
      redirect_to result_path(params[:search],params[:location])
    end
  end

  def result
    @job_pages = JobPages.all
    @posts = Post.all.order_by(created_at: :desc).limit(3)
    @advertising = Advertising.all.order_by(order: :desc).limit(3)
    @jobpages = JobPages.all

    session[:search] = params[:search]
    session[:location] = params[:location]

    if session[:search].to_s.match(/\s/)
      session[:search] = session[:search].to_s.gsub!(" ", "+")
    end

    if session[:location].to_s.match(/\s/)
      session[:location] = session[:location].to_s.gsub!(" ", "+")
    end

    @url="http://api.simplyhired.com/a/jobs-api/xml-v2/q-"+session[:search].to_s+"/l-"+session[:location].to_s+"?pshid=68487&ssty=2&cflg=r&jbd=jobgoal.jobamatic.com"

    @doc = Nokogiri::HTML(open(@url))
    @result = @doc.css("rs r").to_s
    @total_result = @doc.css("tr").to_s
    @total_result_2 = @doc.css("tr").to_s
    @current_page_count = @doc.css("si").to_s

    if @total_result.nil?
      @total_result
    else
      @total_result = @total_result
    end



  end

  def next_result
    @job_pages = JobPages.all
    session[:search] = params[:search]
    session[:location] = params[:location]


    if params[:page].present?
      @page = params[:page]
    end

    @posts = Post.all.order_by(created_at: :desc).limit(3)

    @advertising = Advertising.all.desc('order').limit(3)

    if session[:search].to_s.match(/\s/)
      session[:search] = session[:search].to_s.gsub!(" ", "+")
    end

    if session[:location].to_s.match(/\s/)
      session[:location] = session[:location].to_s.gsub!(" ", "+")
    end

    @url="http://api.simplyhired.com/a/jobs-api/xml-v2/q-"+session[:search].to_s+"/l-"+session[:location].to_s+"/pn-"+@page.to_s+"?pshid=68487&ssty=2&cflg=r&jbd=jobgoal.jobamatic.com"

    @doc = Nokogiri::HTML(open(@url))
    @result = @doc.css("rs r").to_s
    @total_result = @doc.css("tr").to_s
    @current_page_count = @doc.css("si").to_s

    if @total_result.nil? || @current_page_count.nil?
      render(text:'404')
    else
      render(text:'lol')
    end

    if @total_result.nil?
      @total_result
    end

  end




  def sorting
    @job_pages = JobPages.all
    if params[:sort_by].present?
      session[:sort_by] =params[:sort_by]
    end

    if params[:miles].present?
      session[:miles] =  params[:miles]
    end

    if params[:job_source].present?
      params[:job_source] = params[:job_source]
    end

    @posts = Post.all.order_by(created_at: :desc).limit(3)
    @advertising = Advertising.all.desc('order').limit(3)

    if session[:search].to_s.match(/\s/)
      session[:search] = session[:search].to_s.gsub!(" ", "+")
    end

    if session[:location].to_s.match(/\s/)
      session[:location] = session[:location].to_s.gsub!(" ", "+")
    end


    @url="http://api.simplyhired.com/a/jobs-api/xml-v2/q-"+session[:search].to_s+"/l-"+session[:location].to_s+"/pn-"+session[:page].to_s+"/sb-"+session[:sort_by].to_s+"/mi-"+session[:miles].to_s+"/fsr-"+session[:job_source].to_s+"?pshid=68487&ssty=2&cflg=r&jbd=jobgoal.jobamatic.com"

    @doc = Nokogiri::HTML(open(@url))
    @result = @doc.css("rs r").to_s
    @total_result = @doc.css("tr").to_s
    @current_page_count = @doc.css("si").to_s

    if @total_result.nil?
      @total_result
    else
      @total_result = @total_result
    end

    $page = params[:page]

    if $page.nil?
      $page='1'
    else
      # redirect_to 'http://www.simplyhired.com/k-'+$search+'-jobs.html'
      redirect_to next_result_path
    end
  end

  def advanced_search
    @job_pages = JobPages.all
    session[:search] = params[:search]
    session[:location] = params[:location]
   # With All Words
    #
    #
    #
    if params[:with_all_words].present?
      session[:with_all_words] = params[:with_all_words]+"+"

      if session[:with_all_words].to_s.match(/\s/)
        session[:with_all_words] = session[:with_all_words].to_s.gsub!(" ", "+AND+")
      end

    else
      session[:with_all_words] = ''
    end

    if params[:extract_phrase].present?
      session[:extract_phrase] = params[:extract_phrase]+"+"

      if  session[:extract_phrase].to_s.match(/\s/)
        session[:extract_phrase] =  session[:extract_phrase].to_s.gsub!(" ", "+")
      end

    else
      session[:extract_phrase] = ''
    end

    if params[:extract_phrase_company].present?
      session[:extract_phrase_company] = params[:extract_phrase_company]+"+"

      if session[:extract_phrase_company].to_s.match(/\s/)
        session[:extract_phrase_company] = session[:extract_phrase_company].to_s.gsub!(" ", "+")
      end

    else
      session[:extract_phrase_company] = ''
    end

    if params[:without_words].present?
      session[:without_words] = params[:without_words]+"+"

      if session[:without_words].to_s.match(/\s/)
        session[:without_words] = session[:without_words].to_s.gsub!(" ", "+NOT+")
      end

    else
      session[:without_words] = ''
    end

    if params[:least_one_of_words].present?
      session[:least_one_of_words] = params[:least_one_of_words]+"+"

      if session[:least_one_of_words].to_s.match(/\s/)
        session[:least_one_of_words] = session[:least_one_of_words].to_s.gsub!(" ", "+OR+")
      end
    else
      session[:least_one_of_words] = ''
    end

    if params[:state].present?
      session[:state] = params[:state]+"+"

     # if $least_one_of_words.to_s.match(/\s/)
     #   $least_one_of_words = $least_one_of_words.to_s.gsub!(" ", "+OR+")
     # end
    else
      session[:state] = ''
    end

    if params[:state_city].present?
      session[:state_city] = params[:state_city]+"+"

    # if $least_one_of_words.to_s.match(/\s/)
    #   $least_one_of_words = $least_one_of_words.to_s.gsub!(" ", "+OR+")
    # end

    if session[:state_city].to_s.match(/\s/)
      session[:state_city] = session[:state_city].to_s.gsub!(" ", "+")
    end
  else
    session[:state_city] = ''
  end

    if params[:state_zip].present?
      session[:state_zip] = params[:state_zip]+"+"

      # if $least_one_of_words.to_s.match(/\s/)
      #   $least_one_of_words = $least_one_of_words.to_s.gsub!(" ", "+OR+")
      # end

      if session[:state_zip].to_s.match(/\s/)
        session[:state_zip] = session[:state_zip].to_s.gsub!(" ", "+")
      end
    else
      session[:state_zip] = ''
    end

    if params[:miles].present?
      session[:miles] = params[:miles]

    else
      session[:miles] = ''
    end

    if params[:result_per_page].present?
      session[:result_per_page] = params[:result_per_page]

      else
        session[:result_per_page] = ''
      end

    if params[:sort_by_adv].present?
      session[:sort_by_adv] = params[:sort_by_adv]

    else
      session[:sort_by_adv] = ''
    end

    @url="http://api.simplyhired.com/a/jobs-api/xml-v2/q-"+session[:least_one_of_words].to_s+"?pshid=68487&ssty=2&cflg=r&jbd=jobgoal.jobamatic.com"

    @doc = Nokogiri::HTML(open(@url))
    @result = @doc.css("rs r").to_s
    @total_result = @doc.css("tr").to_s
    @current_page_count = @doc.css("si").to_s

    if params[:with_all_words].present? || params[:least_one_of_words].present? || params[:extract_phrase].present? || params[:extract_phrase_company].present? || params[:without_words].present? || params[:state].present? || params[:state_city].present? || params[:state_zip].present? || params[:miles].present? || params[:result_per_page].present? || params[:sort_by_adv].present?
      if @doc.nil?
        redirect_to root_path
      else
        redirect_to advanced_search_result_path
      end
    end

    #
    #
    #
   # End With All Words

  end

  def post_job
    @job_pages = JobPages.all
    @posts = Post.all.order_by(created_at: :desc).limit(3)
    @advertising = Advertising.all.order_by(order: :desc).limit(3)
  end

  def advanced_search_result
    @job_pages = JobPages.all
    @posts = Post.all.order_by(created_at: :desc).limit(3)

    @advertising = Advertising.all.desc('order').limit(3)

    @url="http://api.simplyhired.com/a/jobs-api/xml-v2/q-"+session[:with_all_words].to_s+session[:extract_phrase].to_s+session[:extract_phrase_company].to_s+session[:without_words].to_s+session[:least_one_of_words].to_s+"/l-"+session[:state].to_s+session[:state_city].to_s+session[:state_zip].to_s+"/mi-"+session[:miles].to_s+"/ws-"+session[:result_per_page].to_s+"/sb-"+session[:sort_by_adv].to_s+"?pshid=68487&ssty=2&cflg=r&jbd=jobgoal.jobamatic.com"

    @doc = Nokogiri::HTML(open(@url))
    @result = @doc.css("rs r").to_s
    @total_result = @doc.css("tr").to_s
    @current_page_count = @doc.css("si").to_s

  end

  def job_listing
    @job_pages = JobPages.all

   # if params[:search].present?
   #   session[:search] = params[:search]
   #   if session[:search].to_s.match(/\s/)
   #     session[:search] = session[:search].to_s.gsub!(" ", "+")
   #   end
   # end
  end

  def show
    @job_pages = JobPages.all
  end

  def show_page
    @job_pages = JobPages.all
  end

  def contact
      @job_pages = JobPages.all
      @name = params[:c_name]
      @email = params[:c_email]
      @subject = params[:c_subject]
      @message = params[:c_message]

    if @name.present? && verify_recaptcha
        ContactMailer.feedback(@name,@email,@subject,@message).deliver
        redirect_to root_path
    end

  end

  private

  def set_post
    @post = Post.find(params[:id])
  end
  def set_page
    @job_page = JobPages.find(params[:id])
  end


end
