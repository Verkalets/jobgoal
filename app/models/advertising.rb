class Advertising
  include Mongoid::Document
  include Mongoid::Timestamps
  field :title, type: String
  field :company, type: String
  field :city, type: String
  field :text, type: String
  field :date, type: Date
  field :link, type: String
  field :order, type: Integer

  validates_uniqueness_of :order
  validates_presence_of :order
end
