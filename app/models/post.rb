class Post
  include Mongoid::Document
  include Mongoid::Timestamps
  field :title, type: String
  field :description, type: String
  field :content, type: String
  field :permalink, type: String
  validates_presence_of :permalink, :title, :description, :content
  validates_uniqueness_of :permalink
end
