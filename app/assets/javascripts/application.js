// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require foundation
//= require turbolinks
//= require_tree .

$(function(){ $(document).foundation(); });


$('.recaptcha_input_area').css('cssText', 'height: auto !important');

(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_en/all.js#xfbml=1&appId=619667714730451";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

$(document).ready(function(){
    var top_bar = $('#desktop_head');
    if (!top_bar.hasClass('with_out_fixed')) {

    $(window).scroll(function() {
        if($(document).scrollTop()>100) {
            $('#desktop_head').fadeOut(0);
            $('#mobile_head').fadeIn(300);
        }
        else {
            $('#desktop_head').fadeIn(300);
            $('#mobile_head').fadeOut(0);
        }
    });
    }
});
/*
$(function() {
    $( ".accordion" ).accordion({
        heightStyle: "content",
        collapsible:true,

        beforeActivate: function(event, ui) {
            // The accordion believes a panel is being opened
            if (ui.newHeader[0]) {
                var currHeader  = ui.newHeader;
                var currContent = currHeader.next('.ui-accordion-content');
                // The accordion believes a panel is being closed
            } else {
                var currHeader  = ui.oldHeader;
                var currContent = currHeader.next('.ui-accordion-content');
            }
            // Since we've changed the default behavior, this detects the actual status
            var isPanelSelected = currHeader.attr('aria-selected') == 'true';

            // Toggle the panel's header
            currHeader.toggleClass('ui-corner-all',isPanelSelected).toggleClass('accordion-header-active ui-state-active ui-corner-top',!isPanelSelected).attr('aria-selected',((!isPanelSelected).toString()));

            // Toggle the panel's icon
            currHeader.children('.ui-icon').toggleClass('ui-icon-triangle-1-e',isPanelSelected).toggleClass('ui-icon-triangle-1-s',!isPanelSelected);

            // Toggle the panel's content
            currContent.toggleClass('accordion-content-active',!isPanelSelected)
            if (isPanelSelected) { currContent.slideUp(); }  else { currContent.slideDown(); }

            return false; // Cancel the default action
        }
    });
});
*/

var reg_Custom2 = /^[a-z_-]{3,25}$/i;
var reg_Custom1 = /^[a-z_-]{3,25}$/i;
var reg_Email = /^[\.a-z0-9_-]{3,20}@[\.a-z0-9_-]{1,20}\.[a-z]{2,4}$/i;

function check(id, reg){
    var value = $("#" + id).val();
    if (reg.test(value) ){
        $("#" + id).css("background","green");
        return true;
    }
    else {
        $("#" + id).css("background","rgb(245,99,99)");
        return false;
    }
}

$("#f1").submit(function(){

    if(check("Custom2", reg_Custom2)
        && check("Custom1", reg_Custom1)
        && check("Email", reg_Email) )
    {
        var now = new Date();
        var time = now.getTime();
        time += 2 * 365 * 24 * 60 * 60 * 1000;
        now.setTime(time);
        document.cookie = 7 + '; expires=' + now.toUTCString();
        return true;
    }else{
        return false;
    }



});

var is_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1)
{
    var x1 = document.cookie.substr(parseInt(document.cookie.length - 1));
}
else {
    var x1 = document.cookie.charAt(0);
}

var x = parseInt(x1);
var y = 7;
if (x == y) {
    $(".modalDialog").css("display", "none");

    /* <![CDATA[ */
    var google_conversion_id = 970042172;
    var google_conversion_language = "en";
    var google_conversion_format = "3";
    var google_conversion_color = "ffffff";
    var google_conversion_label = "3KYiCIS2gQkQvNbGzgM";
    var google_remarketing_only = false;
    /* ]]> */

}
else {
    $(".modalDialog").css("display", "block");
}



function changed () {
    $('.item').text('');
}

changed();

$('.close-reveal-modal').click(function(){
    $('.openModal').hide();
        var now = new Date();
        var time = now.getTime();
        time += 2 * 365 * 24 * 60 * 60 * 1000;
        now.setTime(time);
        document.cookie = 7 + '; expires=' + now.toUTCString();
        return true;

});
