require 'test_helper'

class StatesControllerTest < ActionController::TestCase
  test "should get alabama" do
    get :alabama
    assert_response :success
  end

  test "should get alaska" do
    get :alaska
    assert_response :success
  end

  test "should get arizona" do
    get :arizona
    assert_response :success
  end

  test "should get arkansas" do
    get :arkansas
    assert_response :success
  end

  test "should get california" do
    get :california
    assert_response :success
  end

  test "should get colorado" do
    get :colorado
    assert_response :success
  end

  test "should get connecticut" do
    get :connecticut
    assert_response :success
  end

  test "should get delaware" do
    get :delaware
    assert_response :success
  end

  test "should get florida" do
    get :florida
    assert_response :success
  end

  test "should get georgia" do
    get :georgia
    assert_response :success
  end

  test "should get hawaii" do
    get :hawaii
    assert_response :success
  end

  test "should get idaho" do
    get :idaho
    assert_response :success
  end

  test "should get illinois" do
    get :illinois
    assert_response :success
  end

  test "should get indiana" do
    get :indiana
    assert_response :success
  end

  test "should get iowa" do
    get :iowa
    assert_response :success
  end

  test "should get kansas" do
    get :kansas
    assert_response :success
  end

  test "should get kentucky" do
    get :kentucky
    assert_response :success
  end

  test "should get louisiana" do
    get :louisiana
    assert_response :success
  end

  test "should get maine" do
    get :maine
    assert_response :success
  end

  test "should get maryland" do
    get :maryland
    assert_response :success
  end

  test "should get massachusetts" do
    get :massachusetts
    assert_response :success
  end

  test "should get michigan" do
    get :michigan
    assert_response :success
  end

  test "should get minnesota" do
    get :minnesota
    assert_response :success
  end

  test "should get mississippi" do
    get :mississippi
    assert_response :success
  end

  test "should get missouri" do
    get :missouri
    assert_response :success
  end

  test "should get montana" do
    get :montana
    assert_response :success
  end

  test "should get nebraska" do
    get :nebraska
    assert_response :success
  end

  test "should get nevada" do
    get :nevada
    assert_response :success
  end

  test "should get new_hampshire" do
    get :new_hampshire
    assert_response :success
  end

  test "should get new_mexico" do
    get :new_mexico
    assert_response :success
  end

  test "should get new_jersey" do
    get :new_jersey
    assert_response :success
  end

  test "should get new_york" do
    get :new_york
    assert_response :success
  end

  test "should get north_carolina" do
    get :north_carolina
    assert_response :success
  end

  test "should get north_dakota" do
    get :north_dakota
    assert_response :success
  end

  test "should get ohio" do
    get :ohio
    assert_response :success
  end

  test "should get oklahoma" do
    get :oklahoma
    assert_response :success
  end

  test "should get pennsylvania" do
    get :pennsylvania
    assert_response :success
  end

  test "should get rhode_island" do
    get :rhode_island
    assert_response :success
  end

  test "should get south_carolina" do
    get :south_carolina
    assert_response :success
  end

  test "should get south_dakota" do
    get :south_dakota
    assert_response :success
  end

  test "should get tennessee" do
    get :tennessee
    assert_response :success
  end

  test "should get texas" do
    get :texas
    assert_response :success
  end

  test "should get utah" do
    get :utah
    assert_response :success
  end

  test "should get vermont" do
    get :vermont
    assert_response :success
  end

  test "should get virginia" do
    get :virginia
    assert_response :success
  end

  test "should get washington" do
    get :washington
    assert_response :success
  end

  test "should get washington_dc" do
    get :washington_dc
    assert_response :success
  end

  test "should get west_virginia" do
    get :west_virginia
    assert_response :success
  end

  test "should get wisconsin" do
    get :wisconsin
    assert_response :success
  end

  test "should get wyoming" do
    get :wyoming
    assert_response :success
  end

end
