require 'test_helper'

class IndustryControllerTest < ActionController::TestCase
  test "should get forestry" do
    get :forestry
    assert_response :success
  end

  test "should get hunting" do
    get :hunting
    assert_response :success
  end

  test "should get automotive" do
    get :automotive
    assert_response :success
  end

  test "should get banking" do
    get :banking
    assert_response :success
  end

  test "should get computing" do
    get :computing
    assert_response :success
  end

  test "should get technology" do
    get :technology
    assert_response :success
  end

  test "should get defense" do
    get :defense
    assert_response :success
  end

  test "should get energy" do
    get :energy
    assert_response :success
  end

  test "should get arts" do
    get :arts
    assert_response :success
  end

  test "should get food" do
    get :food
    assert_response :success
  end

  test "should get health" do
    get :health
    assert_response :success
  end

  test "should get insurance" do
    get :insurance
    assert_response :success
  end

  test "should get manufacturing" do
    get :manufacturing
    assert_response :success
  end

  test "should get marketing" do
    get :marketing
    assert_response :success
  end

  test "should get media" do
    get :media
    assert_response :success
  end

  test "should get mining" do
    get :mining
    assert_response :success
  end

  test "should get personal" do
    get :personal
    assert_response :success
  end

  test "should get services" do
    get :services
    assert_response :success
  end

  test "should get biotechnology" do
    get :biotechnology
    assert_response :success
  end

  test "should get services" do
    get :services
    assert_response :success
  end

  test "should get estate" do
    get :estate
    assert_response :success
  end

  test "should get retail" do
    get :retail
    assert_response :success
  end

  test "should get transportation" do
    get :transportation
    assert_response :success
  end

  test "should get travel" do
    get :travel
    assert_response :success
  end

  test "should get waste" do
    get :waste
    assert_response :success
  end

  test "should get remediation" do
    get :remediation
    assert_response :success
  end

end
