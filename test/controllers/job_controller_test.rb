require 'test_helper'

class JobControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get result" do
    get :result
    assert_response :success
  end

  test "should get home" do
    get :home
    assert_response :success
  end

  test "should get advanced_search" do
    get :advanced_search
    assert_response :success
  end

  test "should get post_job" do
    get :post_job
    assert_response :success
  end

end
