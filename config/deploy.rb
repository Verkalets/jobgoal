set :application, "jobgoal.net"
set :domain, "deploy@jobgoal.net"
set :deploy_to, "/home/deploy/#{application}"
set :use_sudo, false
set :unicorn_conf, "#{deploy_to}/current/config/unicorn.rb"
set :unicorn_pid, "#{deploy_to}/shared/pids/unicorn.pid"

set :rvm_ruby_string, 'ree'

set :scm, :git
set :repository,  "https://bitbucket.org/Verkalets/jobgoal.git"
set :branch, "master"
set :deploy_via, :remote_cache

role :web, domain
role :app, domain
role :db,  domain, :primary => true

#before 'deploy:setup', 'rvm:install_rvm', 'rvm:install_ruby'

#after 'deploy:update_code', :roles => :app do
#  run "rm -f #{current_release}/config/database.yml"
#  run "ln -s #{deploy_to}/shared/config/database.yml #{current_release}/config/database.yml"
#end

namespace :deploy do
  task :restart do
    run "if [ -f #{unicorn_pid} ] && [ -e /proc/$(cat #{unicorn_pid}) ]; then kill -USR2 `cat #{unicorn_pid}`; else cd #{deploy_to}/current && bundle exec unicorn_rails -c #{unicorn_conf} -D; fi"
  end
  task :start do
    run "cd #{deploy_to}/current/ && bundle exec unicorn_rails -c #{unicorn_conf} -D"
  end
  task :stop do
    run "if [ -f #{unicorn_pid} ] && [ -e /proc/$(cat #{unicorn_pid}) ]; then kill -QUIT `cat #{unicorn_pid}`; fi"
  end
end