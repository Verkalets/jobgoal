Jobgoal::Application.routes.draw do

  get "ameriplan/sales-rep" => 'ameriplan#sales'
  get 'ameriplan/rssfeed'
  get "industry/forestry"
  get "industry/hunting"
  get "industry/automotive"
  get "industry/banking"
  get "industry/computing"
  get "industry/technology"
  get "industry/defense"
  get "industry/energy"
  get "industry/arts"
  get "industry/food"
  get "industry/health"
  get "industry/insurance"
  get "industry/manufacturing"
  get "industry/marketing"
  get "industry/media"
  get "industry/mining"
  get "industry/personal"
  get "industry/biotechnology"
  get "industry/services"
  get "industry/estate"
  get "industry/retail"
  get "industry/transportation"
  get "industry/travel"
  get "industry/waste"
  get "industry/remediation"

  get "states/alabama"
  get "states/alaska"
  get "states/arizona"
  get "states/arkansas"
  get "states/california"
  get "states/colorado"
  get "states/connecticut"
  get "states/delaware"
  get "states/florida"
  get "states/georgia"
  get "states/hawaii"
  get "states/idaho"
  get "states/illinois"
  get "states/indiana"
  get "states/iowa"
  get "states/kansas"
  get "states/kentucky"
  get "states/louisiana"
  get "states/maine"
  get "states/maryland"
  get "states/massachusetts"
  get "states/michigan"
  get "states/minnesota"
  get "states/mississippi"
  get "states/missouri"
  get "states/montana"
  get "states/nebraska"
  get "states/nevada"
  get "states/new_hampshire"
  get "states/new_mexico"
  get "states/new_jersey"
  get "states/new_york"
  get "states/north_carolina"
  get "states/north_dakota"
  get "states/ohio"
  get "states/oklahoma"
  get "states/pennsylvania"
  get "states/rhode_island"
  get "states/south_carolina"
  get "states/south_dakota"
  get "states/tennessee"
  get "states/texas"
  get "states/utah"
  get "states/vermont"
  get "states/virginia"
  get "states/washington"
  get "states/west_virginia"
  get "states/wisconsin"
  get "states/wyoming"

  devise_for :users



  mount RailsAdmin::Engine => '/job_goal_admin', :as => 'rails_admin'


  get 'landing' => 'job#landing'
  get 'home' => 'job#home', as: :home
  get 'contact' => 'job#contact', as: :contact

  root 'job#home'

  get "post_job" => 'job#post_job', as: :post_job



  get 'advanced_search' => 'job#advanced_search', as: :advanced_search
  get 'advanced_search_result' => 'job#advanced_search_result', as: :advanced_search_result

  get 'sorting' => 'job#sorting', as: :sorting
  get 'job_listing' => 'job#job_listing', as: :job_listing


  # Dinamic params

  get "post/:id", to: 'job#show', as: :post
  get "page/:id", to: 'job#show_page', as: :page


  get ':search/:location' => 'job#result' , as: :result, via: [:get]
  get ':search' => 'job#search_result' , as: :search_result, via: [:get]
  get '/location/:location' => 'job#location_result' , as: :location_result, via: [:get]
  get 'job/:search/:location' => 'job#next_result', as: :next_result, via: [:get]
end
